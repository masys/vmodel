'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var axios = require('axios');

var _require = require('lodash'),
    get = _require.get;

var VModel = function () {
  _createClass(VModel, null, [{
    key: 'all',
    value: async function all(filter) {
      if (this.stringifyParams) {
        filter = JSON.stringify(filter);
      }

      var _ref = await this._get(this.url, filter),
          data = _ref.data;

      return this._mapResponse(data);
    }
  }, {
    key: 'create',
    value: async function create(payload) {
      var obj = {};
      if (this.paramKey) {
        obj[this.paramKey] = payload;
      } else {
        obj = payload;
      }

      var _ref2 = await this._post(this.url, obj),
          data = _ref2.data;

      return new this(data.data || data);
    }
  }, {
    key: 'findOne',
    value: async function findOne(id) {
      var filter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (this.stringifyParams) {
        filter = JSON.stringify(filter);
      }

      var _ref3 = await this._get(this.url + ('/' + id), filter),
          data = _ref3.data;

      return new this(data.data || data);
    }
  }, {
    key: 'count',
    value: async function count() {
      var where = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (this.stringifyParams) {
        where = JSON.stringify(where);
      }

      var _ref4 = await this._get(this.url + '/count', { where: where }),
          data = _ref4.data;

      return data.data || data;
    }
  }, {
    key: 'destroyById',
    value: async function destroyById(id) {
      var _ref5 = await this._delete(this.url + '/' + id),
          data = _ref5.data;

      return new this(data);
    }
  }, {
    key: '_get',
    value: async function _get(url, params) {
      return this.http.get(url, {
        params: params,
        headers: this.headers
      });
    }
  }, {
    key: '_post',
    value: async function _post(url, params) {
      return this.http.post(url, params, {
        headers: this.headers
      });
    }
  }, {
    key: '_patch',
    value: async function _patch(url, params) {
      return this.http.patch(url, params, {
        headers: this.headers
      });
    }
  }, {
    key: '_put',
    value: async function _put(url, params) {
      return this.http.put(url, params, {
        headers: this.headers
      });
    }
  }, {
    key: '_delete',
    value: async function _delete(url) {
      return this.http.delete(url, {
        headers: this.headers
      });
    }
  }, {
    key: 'setStorage',
    value: function setStorage(name, payload) {
      window[this.storageType].setItem(name, JSON.stringify(payload));
    }
  }, {
    key: 'getStorage',
    value: function getStorage(name) {
      var item = window[this.storageType].getItem(name);

      return JSON.parse(item);
    }
  }, {
    key: 'deleteStorage',
    value: function deleteStorage(name) {
      window[this.storageType].removeItem(name);
    }
  }, {
    key: '_mapResponse',
    value: function _mapResponse(response) {
      var _this = this;

      var data = response.data || response;
      var collection = data.map(function (o) {
        return new _this(o);
      });
      collection.pagination = get(response, 'meta.pagination');
      return collection;
    }

    // Prototype -------------------------------------------------------------------------------------

  }, {
    key: 'url',
    get: function get() {
      throw new Error('Pending Implementation!');
    }
  }, {
    key: 'paramKey',
    get: function get() {
      return null;
    }
  }, {
    key: 'accessToken',
    get: function get() {
      var user = this.getStorage(this.storageName) || {};

      return user[this.accessTokenKey];
    }
  }, {
    key: 'headers',
    get: function get() {
      var headers = {};
      if (this.accessToken) {
        headers['Authorization'] = this.authorizationString.replace('{accessToken}', this.accessToken);
      }

      return headers;
    }
  }]);

  function VModel() {
    var attrs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, VModel);

    this._setAttributes(attrs);
  }

  _createClass(VModel, [{
    key: '_setAttributes',
    value: function _setAttributes(attrs) {
      var _this2 = this;

      if (!attrs) {
        return;
      }

      Object.defineProperty(this, 'oldValues', {
        value: attrs,
        enumerable: false,
        writable: false,
        configurable: true
      });

      Object.keys(attrs).forEach(function (key) {
        _this2._setAttribute(key, attrs[key]);
      });
    }
  }, {
    key: '_setAttribute',
    value: function _setAttribute(key, value) {
      this[key] = value;
      Object.defineProperty(this, key, {
        value: value,
        enumerable: true,
        writable: true
      });
    }
  }, {
    key: 'save',
    value: async function save() {
      if (this.id) {
        return this.update();
      } else {
        return this.create();
      }
    }
  }, {
    key: 'create',
    value: async function create() {
      var payload = {};
      if (this.constructor.paramKey) {
        payload[this.constructor.paramKey] = _extends({}, this);
      } else {
        payload = _extends({}, this);
      }

      var _ref6 = await this.constructor._post(this.constructor.url, payload),
          data = _ref6.data;

      if (data.data) {
        this._setAttributes(data.data);
      } else {
        this._setAttributes(data);
      }

      return true;
    }
  }, {
    key: 'update',
    value: async function update() {
      var payload = {};
      if (this.constructor.paramKey) {
        payload[this.constructor.paramKey] = this.attributesToUpdate();
        if (!Object.keys(payload[this.constructor.paramKey]).length) {
          return true;
        }
      } else {
        payload = this.attributesToUpdate();
        if (!Object.keys(payload).length) {
          return true;
        }
      }

      var res = void 0;
      if (this.constructor.updateWithPatch) {
        res = await this.constructor._patch(this.constructor.url + ('/' + this.id), payload);
      } else {
        res = await this.constructor._put(this.constructor.url + ('/' + this.id), payload);
      }
      if (res.data.data) {
        this._setAttributes(res.data.data);
      } else {
        this._setAttributes(res.data);
      }

      return true;
    }
  }, {
    key: 'attributesToUpdate',
    value: function attributesToUpdate() {
      if (this.constructor.sendAllAttributesOnUpdate) {
        return _extends({}, this);
      }

      var attrs = {};
      var keys = Object.keys(this);
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = keys[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var key = _step.value;

          if (this[key] !== this.oldValues[key]) {
            attrs[key] = this[key];
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return attrs;
    }
  }]);

  return VModel;
}();

VModel.apiUrl = process.env.VUE_APP_API_URL;
VModel.http = axios;
VModel.authorizationString = 'Token token={accessToken}';
VModel.storageType = 'localStorage';
VModel.storageName = (process.env.NODE_ENV || '')[0] || 'dev';
VModel.accessTokenKey = 'token';
VModel.stringifyParams = false;
VModel.sendAllAttributesOnUpdate = false;
VModel.updateWithPatch = true;
VModel.onUnauthorized = function () {
  undefined.deleteStorage(undefined.storageName);
};

exports.default = VModel;