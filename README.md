```javascript
// plugins/vmodel.js

import VModel from 'vmodel'

VModel.apiUrl = process.env.VUE_APP_API_URL
VModel.http = axios
VModel.authorizationString = 'Token token={accessToken}'
VModel.storageType = 'localStorage'
VModel.storageName = (process.env.NODE_ENV || '')[0] || 'dev'
VModel.accessTokenKey = 'token'
VModel.stringifyParams = false
VModel.sendAllAttributesOnUpdate = false
```

```javascript
// client/user.js

import VModel from 'vmodel'

class User extends VModel {
  static get url () {
    return '/users' // VModel.apiUrl + /users
  }
}
```

usage:

```javascript
const user = new User({ name: 'John', last_name: 'Doe' })
await user.save() // will call the API

// Get list of users
const users = await User.all(filterObject)

// Find user by id
const user = await User.findOne(id, filter)

// Destroy User
await User.destroyById(id)

VModel.setStorage('key', 'value')
VModel.getStorage('key')
VModel.deleteStorage('key')
```
