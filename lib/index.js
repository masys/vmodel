const axios = require('axios')
const { get } = require('lodash')

class VModel {

  static get url () {
    throw new Error('Pending Implementation!')
  }

  static get paramKey () {
    return null
  }

  static get accessToken () {
    const user = this.getStorage(this.storageName) || {}

    return user[this.accessTokenKey]
  }

  static get headers () {
    const headers = {}
    if (this.accessToken) {
      headers['Authorization'] = this.authorizationString.replace('{accessToken}', this.accessToken)
    }

    return headers
  }

  static async all (filter) {
    if (this.stringifyParams) {
      filter = JSON.stringify(filter)
    }
    const { data } = await this._get(this.url, filter)

    return this._mapResponse(data)
  }

  static async create (payload) {
    let obj = {}
    if (this.paramKey) {
      obj[this.paramKey] = payload
    } else {
      obj = payload
    }
    const { data } = await this._post(this.url, obj)

    return new this(data.data || data)
  }

  static async findOne (id, filter = {}) {
    if (this.stringifyParams) {
      filter = JSON.stringify(filter)
    }
    const { data } = await this._get(this.url + `/${id}`, filter)

    return new this(data.data || data)
  }

  static async count (where = {}) {
    if (this.stringifyParams) {
      where = JSON.stringify(where)
    }
    const { data } = await this._get(this.url + '/count', { where })

    return data.data || data
  }

  static async destroyById (id) {
    const { data } = await this._delete(`${this.url}/${id}`)

    return new this(data)
  }

  static async _get (url, params) {
    return this.http.get(url, {
      params,
      headers: this.headers
    })
  }

  static async _post (url, params) {
    return this.http.post(url, params, {
      headers: this.headers
    })
  }

  static async _patch (url, params) {
    return this.http.patch(url, params, {
      headers: this.headers
    })
  }

  static async _put (url, params) {
    return this.http.put(url, params, {
      headers: this.headers
    })
  }

  static async _delete (url) {
    return this.http.delete(url, {
      headers: this.headers
    })
  }

  static setStorage (name, payload) {
    window[this.storageType].setItem(name, JSON.stringify(payload))
  }

  static getStorage (name) {
    const item = window[this.storageType].getItem(name)

    return JSON.parse(item)
  }

  static deleteStorage (name) {
    window[this.storageType].removeItem(name)
  }

  static _mapResponse (response) {
    const data = response.data || response
    const collection = data.map(o => new this(o))
    collection.pagination = get(response, 'meta.pagination')
    return collection
  }

  // Prototype -------------------------------------------------------------------------------------

  constructor (attrs = {}) {
    this._setAttributes(attrs)
  }

  _setAttributes (attrs) {
    if (!attrs) { return }

    Object.defineProperty(this, 'oldValues', {
      value: attrs,
      enumerable: false,
      writable: false,
      configurable: true
    })

    Object.keys(attrs).forEach(key => {
      this._setAttribute(key, attrs[key])
    })
  }

  _setAttribute (key, value) {
    this[key] = value
    Object.defineProperty(this, key, {
      value: value,
      enumerable: true,
      writable: true
    })
  }

  async save () {
    if (this.id) {
      return this.update()
    } else {
      return this.create()
    }
  }

  async create () {
    let payload = {}
    if (this.constructor.paramKey) {
      payload[this.constructor.paramKey] = { ...this }
    } else {
      payload = { ...this }
    }
    const { data } = await this.constructor._post(this.constructor.url, payload)
    if (data.data) {
      this._setAttributes(data.data)
    } else {
      this._setAttributes(data)
    }

    return true
  }

  async update () {
    let payload = {}
    if (this.constructor.paramKey) {
      payload[this.constructor.paramKey] = this.attributesToUpdate()
      if (!Object.keys(payload[this.constructor.paramKey]).length) { return true }
    } else {
      payload = this.attributesToUpdate()
      if (!Object.keys(payload).length) { return true }
    }

    let res
    if (this.constructor.updateWithPatch) {
      res = await this.constructor._patch(this.constructor.url + `/${this.id}`, payload)
    } else {
      res = await this.constructor._put(this.constructor.url + `/${this.id}`, payload)
    }
    if (res.data.data) {
      this._setAttributes(res.data.data)
    } else {
      this._setAttributes(res.data)
    }

    return true
  }

  attributesToUpdate () {
    if (this.constructor.sendAllAttributesOnUpdate) {
      return { ...this }
    }

    const attrs = {}
    const keys = Object.keys(this)
    for (let key of keys) {
      if (this[key] !== this.oldValues[key]) {
        attrs[key] = this[key]
      }
    }
    return attrs
  }
}

VModel.apiUrl = process.env.VUE_APP_API_URL
VModel.http = axios
VModel.authorizationString = 'Token token={accessToken}'
VModel.storageType = 'localStorage'
VModel.storageName = (process.env.NODE_ENV || '')[0] || 'dev'
VModel.accessTokenKey = 'token'
VModel.stringifyParams = false
VModel.sendAllAttributesOnUpdate = false
VModel.updateWithPatch = true
VModel.onUnauthorized = () => {
  this.deleteStorage(this.storageName)
}

export default VModel
